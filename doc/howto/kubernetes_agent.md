# GitLab Agent Server (KAS)

If you wish to clone and keep an updated [GitLab Agent for Kubernetes](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent) as part of your GDK, do the following:

1. Install [Bazel](https://www.bazel.build/)

    The recommended way to install Bazel is to use [Bazelisk](https://github.com/bazelbuild/bazelisk). Bazelisk is a version manager for Bazel, much like rbenv for Ruby. See the [installation instructions](https://docs.bazel.build/versions/master/install-bazelisk.html) for Bazelisk.

1. Add the following settings in your `gdk.yml`:

    ```yaml
    gitlab_k8s_agent:
      enabled: true
    ```

1. (Optional) To use the [CI tunnel](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html) functionality, you must:
   1. Enable [NGINX](nginx.md) in HTTPS mode. `kubectl` never sends credentials over a plain
      text connection.
   1. Specify concrete IP addresses for `kas` to listen on. `gdk.yml` looks like this
      (assuming [loopback alias IP](local_network.md#create-loopback-interface) was set up):

   ```yaml
   gitlab_k8s_agent:
     enabled: true
     agent_listen_address: 172.16.123.1:8150
     k8s_api_listen_address: 172.16.123.1:8154
   hostname: gdk.test
   port: 3443
   https:
     enabled: true
   nginx:
     enabled: true
     ssl:
       certificate: gdk.test.pem
       key: gdk.test-key.pem
   ```

1. Run `gdk update` to get `kas` installed as part of GDK.

1. Run `gdk reconfigure` to update various configuration files.

1. You can start GDK with `gdk start`. It prints the URL for `agentk` to use:

    ```plaintext
    => GitLab available at http://127.0.0.1:3000.
    => GitLab Agent Server (KAS) available at grpc://127.0.0.1:8150.
    ```

    If you are using NGINX+HTTPS, the URL would show something like:

    ```plaintext
    => GitLab available at https://gdk.test:3443.
    => GitLab Agent Server (KAS) available at wss://gdk.test:3443/-/kubernetes-agent.
    ```

1. To verify that `kas` is running you can:
    - Run `gdk tail gitlab-k8s-agent` to check the logs. You should see no errors in the logs. Empty logs are normal too.
    - Run `curl 127.0.0.1:8150`. It should print

        ```plaintext
        Warning: Binary output can mess up your terminal. Use "--output -" to tell
        Warning: curl to output it to your terminal anyway, or consider "--output
        Warning: <FILE>" to save to a file.
        ```

        This is normal because gRPC is a binary protocol.

    - If running with NGINX enabled, run using the loopback address: `curl 172.16.123.1:8150`. It should print

        ```plaintext
        WebSocket protocol violation: Connection header "close" does not contain Upgrade
        ```

        This is a normal response from `kas` for such a request because it's expecting a WebSocket connection upgrade.

1. Once your GitLab Agent Server is running, you can connect to a Kubernetes cluster by [installing `agentk` to the cluster](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#install-the-agent-in-the-cluster). The `kasAddress` should be the GitLab Agent Server URL outputted when you ran `gdk start` or from the listed URLs when you run `gdk status`.

    - To connect to a Kubernetes cluster on `k3d`, read the instructions below for [deploying `agentk` with `k3d`](#optional-deploy-the-gitlab-agent-agentk-with-k3d)

## (Optional) Connecting your project to `agentk` using CI Tunnel

The GitLab Agent Server communicates with `agentk` through a Kubernetes proxy. You can check the proxy address by running `gdk status`, which will output it as one of the URLs:

```plaintext
=> GitLab available at https://gdk.test:3443.
=> GitLab Agent Server (KAS) available at wss://gdk.test:3443/-/kubernetes-agent.
=> Kubernetes proxy (via KAS) available at https://gdk.test:3443/-/k8s-proxy/.
```

The [GitLab Runner](runner.md) must be authorized to access the `https` address. This is done by adding the NGINX SSL certificate to the relevant places.

### Runner configuration

If your runner is configured with a `docker` executor, you must add your certificate to the volumes in your runner's `config.toml`:

  ```plaintext
  [[runners]]
    name = "GDK local runner"
    url = "https://gdk.test:3443"
    # other config here
    [runners.docker]
    volumes = [
      "path/to/gdk/directory/gdk.test.crt:/etc/ssl/certs/gdk.test.crt",
      # other volumes here
      "/certs/client",
      "/cache"
    ]
  ```

Alternatively, you can set the `certificate-authority` of `agentk`'s Kubernetes cluster:

1. Commit the `gdk.test` certificate into your project that is using CI Tunnel. You can also use the root certificate if there is one, e.g.: the `rootCA.pem` generated by `mkcert`.

    1. Copy the contents of the certificate to the clipboard
    1. Create a `gdk.test.crt` or `rootCA.pem` file in the root directory of your project and paste the contents from the previous step.

1. In the project's `.gitlab-ci.yml`, for steps that need to connect to `agentk` (e.g.: the deploy step), add a command to set the certificate authority of the associated Kubernetes cluster:

    ```shell
    kubectl config set clusters.gitlab.certificate-authority "$(pwd)/gdk.test.crt"
    ```

    OR

    ```shell
    kubectl config set clusters.gitlab.certificate-authority "$(pwd)/rootCA.pem"
    ```

    This reads the reads the certificate that you committed to your project's root directory.

    An example `.gitlab-ci.yml`, extended from [this guide](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html#update-your-gitlab-ciyml-file-to-run-kubectl-commands), looks like this:

    ```yaml
    deploy:
      image:
        name: bitnami/kubectl:latest
        entrypoint: ['']
      script:
        - kubectl config get-contexts
        - kubectl config use-context path/to/project:agentk-name
        - kubectl config set clusters.gitlab.certificate-authority "$(pwd)/gdk.test.crt"
        - kubectl get pods --namespace gitlab-agent
    ```

## (Optional) Deploy the GitLab Agent (agentk) with k3d

1. [Install k3d](https://github.com/rancher/k3d#get).
1. Create a k3d cluster:

   ```shell
   k3d cluster create
   ```

1. Set up a [loopback alias IP](local_network.md#create-loopback-interface). We can use it as the
   listen address so that `agentk` can reach your local GitLab and KAS. Let's assume this is `172.16.123.1`.
   We recommend you also bind your hostname to this address in `/etc/hosts`, by adding the line
   `172.16.123.1 gdk.test` to `/etc/hosts`

   Then update your `gdk.yml` to include these global keys:

   ```yaml
   hostname: gdk.test
   listen_address: "172.16.123.1"
   ```

   This sets the default hostname and listen address for all GDK services, including GitLab.
   For example, with the default ports:

   - GitLab would now be available on `http://gdk.test:3000`.
   - The registry would now be available on `https://gdk.test:5000`.

1. Run `gdk reconfigure` to apply the above change.
1. Deploy `agentk`:

   1. [Register the agent](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab) as you normally would to deploy it to any cluster. Take note of the token.
   1. [Install the Agent to the cluster](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#install-the-agent-in-the-cluster).

      At this step, be sure to use the loopback alias as the KAS address instead of the `gdk.test` URL. This is necessary because `k3d` is running on Docker, which will not be able to resolve `gdk.test`.

      You can do this by changing the `config.kasAddress` in the Helm installation command:

      ```shell
      helm upgrade --install agentk-test gitlab/gitlab-agent \
      --namespace gitlab-agent \
      --create-namespace \
      --set image.tag=v15.4.0 \
      --set config.token=<token generated from the previous step> \
      --set config.kasAddress=grpc://172.16.123.1:8150
      ```

## (Optional) Run `agentk` from source using `support/agentk`

To increase development velocity, GDK includes a script `support/agentk` to run
agentk with `go run`, skipping the build and deploy steps associated with
running `agentk` in a cluster. The script takes a single mandatory argument, the
agent token, and runs the agent in the foreground:

```shell
support/agentk TOKEN [EXTRA ARGS FOR AGENTK]
```

When run in this way, `agentk` implicitly uses the currently selected context in
your kubeconfig, similar to how `kubectl` works.

To pick up new changes to the code, simply restart the script. If you are also
making changes to KAS, consider running [KAS from source as
well](#optional-run-kas-directly-from-source-with-go-run).

When running the script, all arguments following the token get forwarded
directly to `agentk`. For example, to use a different kubeconfig context, you
can use the `--context` flag:

```shell
support/agentk TOKEN --context YOUR_CONTEXT
```

To see the list of available flags, you can run

```shell
support/agentk "" --help
```

## (Optional) Run using Bazel instead of GDK

If you want to run GitLab Agent Server and Agent locally with Bazel instead of GDK, see
the [GitLab Agent documentation](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/developing.md#running-the-agent-locally).

## (Optional) Run KAS directly from source (with `go run`)

When working on KAS, it can be convenient to run directly from source instead of
having to rebuild every time.

```shell
gdk config set gitlab_k8s_agent.run_from_source true
gdk reconfigure
```

That way, changes to the code get picked up immediately every time you restart
KAS:

```shell
gdk restart gitlab-k8s-agent
```

## (Optional) Disable automatic repository updates 

When working on the agent, you probably want to manage the repository on your
own, to avoid having `gdk update` stash your changes:

```shell
gdk config set gitlab_k8s_agent.auto_update false
gdk reconfigure
```

## Troubleshooting

See [Bad CPU type in executable Target](../troubleshooting/apple_mx_machines.md#bad-cpu-type-in-executable-target).
